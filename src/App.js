import React, { Component } from 'react';
import './App.css';
import Field from './components/Field/Field';
import Counter from './components/Counter/Counter';

class App extends Component {
    state = {
    newcell : [],
        tries: 0
};

    generateCell = () => {
        let cell = [];
        for (let i = 0; i < 36; i++) {
           cell.push({hesitem:false, invisibility: false})
        }
        return cell;
    };
    componentDidMount = () => {
        this.setState({newcell: this.hoardCell()});
        this.state.tries = 0;
    };
    hoardCell = (index) => {
        let randomcell = Math.floor(Math.random()*36);
        let hoardcell = this.generateCell();
        hoardcell[randomcell].hesitem = true;

        return hoardcell;
    };
    hideCell = (index) => {
        let newcell = [...this.state.newcell];
        let onecell = {...this.state.newcell[index]};
        onecell.invisibility = true;
        newcell[index] = onecell;
        let tries =this.state.tries;
        tries++;
        this.setState({newcell,tries});
    };

  render() {
    return (
      <div className="App">
       <Field cells={this.state.newcell} click={this.hideCell}/>
        <Counter tries={this.state.tries}/>
          <button className="button" onClick={this.componentDidMount}>RESETЕ</button>
      </div>
    );
  }
}

export default App;

