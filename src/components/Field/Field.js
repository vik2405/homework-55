import React from "react";
import './Field.css';
import Cell from  '../Cell/Cell';
let Field = function (props) {
    return(
        <div className="Field">
            {props.cells.map((cell,index)=>  <Cell classList ={'Cell ' + (cell.invisibility? 'Newcell ': '') + (cell.hesitem ? 'hoardcell ' : '')} click={() => props.click(index)} key ={index}/>)}
        </div>
    )
};
        export default Field;